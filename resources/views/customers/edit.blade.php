@extends('layouts.app')
@section('content')

<h1>Edit customer</h1>

<form method = 'post' action = "{{action('CustomerController@update', $customers->id)}}" >

@csrf
@method('PATCH')

<div class = "form-group">
    <label for = "title" > name toUpdate </label>
    <input type = "text" class = "form-control" name = "name" value="{{$customers->name}}">
</div>

<div class = "form-group">
    <label for = "title" > email toUpdate </label>
    <input type = "text" class = "form-control" name = "email" value="{{$customers->email}}">
</div>

<div class = "form-group">
    <label for = "title" > phone toUpdate </label>
    <input type = "text" class = "form-control" name = "phone" value="{{$customers->phone}}">
</div>

<div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "save">
</div>

</form>



@endsection