<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('customers', 'CustomerController')->middleware('auth');

Route::get('tasks/close/{id}', 'CustomerController@close')->name('close');


Route::get('customers/delete/{id}', 'CustomerController@destroy')->name('delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
